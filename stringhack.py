str = r"olle\nolle\nolle\nolle.\nolle.\nolle\n.\n.\n.\n.\n.\n.\n.\n.\n.\n\n\n\n\nolle\n\n\n.\n."
print "Input: %s" % str
while(1):
  if (r'\n.\n' in str) or (r'\n.\0' in str):
    str = str.replace(r'\n.\n',r'\n')
    continue
  elif r'\n\n' in str:
    str = str.replace(r'\n\n',r'\n')
    continue
  else:
      break
str = str.strip('.')
print "Output: %s" % str