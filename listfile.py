AUTHOR = 'Sanjay Sudarshan <sanjay.sudarshan@gmail.com'
NAME = 'List files in a directory'

DOC = '''\
The List File program lists all the files in given directory.
This also has the ability to list only the files with the particular extension.
'''

import glob
from os import listdir
from os.path import isfile, join, isdir, exists


class ListFiles:

    def __init__(self):
        pass

    def listdir(self, directory):
        """Lists all the files in a given directory.

        Args:
            directory: Directory path to search for files

        Returns: List of files in the given directory path.
        """

        # isdir checks if the directory is present.
        if isdir(directory):
            files = [file for file in listdir(directory)
                    if isfile(join(directory,file))]
            return files
        # exists checks for file path
        elif exists(directory):
            print "\nPlease enter only the directory path and not the file name"
        else:
            print "\nSorry, please enter a valid directory path"

    def search_with_extn(self, directory, extn):
        """Lists all the files with the specific extn in the given
           directory using 'glob'.

        Args:
            directory: Directory path to search for files
            extn: Extension of the file to be searched for in a given directory.

        Returns: List of files in the given directory path.
        """

        searchfor = join(directory, '*.%s' % extn)
        print "\nSearching for '%s'" % searchfor
        # glob is used to find all the pathnames matching a specified pattern
        files = [file for file in glob.glob(searchfor)]

        return files

    def write_to_file(self, directory, files):
        """The list of files is been written to a file.
           Saved file will be names as "files_in_<directory_name>.txt"
           in the local path.

        Args:
        directory: Directory path to search for files
        files: List of files in the particular directory.
        """

        directory.replace('/', '_')
        file_name = 'files_in%s.txt' % directory.replace('/', '_')
        fd = open(file_name, 'w')
        fd.write('List of files in %s\n' % directory)
        fd.write('Number of files = %s\n' % len(files))
        for file in files:
           fd.write('\n%s' % file)
        print "\nList of files has been written to file %s" % file_name


def main():

    print ("\n\nHi there! \n"
           "You are running a program to list all the files in a"
           " directory.\n\n")

    while(1)       
        print "Please enter the below options to see the files."
        print "**> press '1' to list all the files in given directory.\n"
        print ("**> press '2' to list all the files in given directory with"
               " only the file extension type.\n")
        print "   Example: List all files with extn 'txt'."
        print "**> press '3' to Exit.\n"
        input_num = raw_input()
        listobj = ListFiles()

        if input_num == '1':
            print ("  Please enter the full path of the directory you want to"
                  " search")
            directory_name = raw_input()
            print "Searching for files in directory %s" % directory_name
            files = listobj.listdir(directory_name)
            listobj.write_to_file(directory_name, files)
            print "No files in directory %s" % directory_name

        elif input_num == '2':
            print "  You would like to search files with the extension."
            print "  Enter the full directory path."
            directory_name = raw_input()
            print "  Enter the extn type, example 'txt' or 'jpeg'."
            extn = raw_input()
            files = listobj.search_with_extn(directory_name, extn)
            listobj.write_to_file(directory_name, files)

        elif input_num == '3':
            print "Bye for now!"
            break

        else:
            print "\n\nOops! Wrong key entered.\n"


if __name__ == "__main__":
    main()